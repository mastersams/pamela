
#include "my.h"

char		*getHome(const char *user)
{
	struct passwd *pass;
	if ((pass = getpwnam(user)) == NULL)
		return (NULL);
	return (pass->pw_dir);

	if (getpwnam(user) == NULL)
		return (NULL);
	return (getpwnam(user)->pw_dir);
}

char		*getKey(const char *user)
{
	char          *buffer;

	if ((buffer = malloc(sizeof(char) * (513))) == NULL)
		return (NULL);
	if (sprintf(buffer, "%s/%s", getHome(user), ".key") < 0)
		return (NULL);
	return (buffer);
}

static int	openCommands(const char *user)
{
	char		*buffer;

	if ((buffer = malloc(sizeof(char) * (513))) == NULL)
		return (-1);
	if (sprintf(buffer, "mkdir %s/%s", getHome(user), CONTAINER) < 0)
		return (-1);
	if (system(buffer) < 0)
		return (-1);
	free(buffer);

	if ((buffer = malloc(sizeof(char) * (513))) == NULL)
		return (-1);
	if (sprintf(buffer, "mount /dev/mapper/%s_%s %s/%s", user, CONTAINER, getHome(user), CONTAINER) < 0)
		return (-1);
	if (system(buffer) < 0)
		return (-1);
	free(buffer);

	if ((buffer = malloc(sizeof(char) * (513))) == NULL)
		return (-1);
	if (sprintf(buffer, "chown %s %s/%s", user, getHome(user), CONTAINER) < 0)
		return (-1);
	if (system(buffer) < 0)
		return (-1);
	free(buffer);

	return (0);
}

int     createContainer(const char *user, const char *password)
{
	char  *buffer;

	printf("Creating container..\n");
	if ((buffer = malloc(sizeof(char) * (513))) == NULL)
		return (-1);
	if (sprintf(buffer, "dd if=/dev/urandom bs=1M count=100 of=%s", getKey(user)) < 0)
		return (-1);
	if (system(buffer) < 0)
		return (-1);
	free(buffer);

	if (my_crypt(user, password) != 0)
		return (-1);
	if (my_decrypt(user, password) != 0)
		return (-1);

	if ((buffer = malloc(sizeof(char) * (513))) == NULL)
		return (-1);
	if (sprintf(buffer, "mkfs.ext4 /dev/mapper/%s_%s", user, CONTAINER) < 0)
		return (-1);
	if (system(buffer) < 0)
		return (-1);
	free(buffer);

	printf("Container created\n");
	return (openCommands(user));
}

int	openContainer(const char *user, const char *password)
{
	if (getHome(user) == NULL)
	{
		printf("Cannot get home directory\n");
		return (-1);
	}
	if (getKey(user) == NULL)
	{
		printf("Cannot get Cannot get Key directory\n");
		return (-1);
	}
	if (access(getKey(user), F_OK) == -1)
		return (createContainer(user, password));
	printf("Decrypting..\n");
	if (my_decrypt(user, password) != 0)
		return (-1);
	printf("Container decrypted\n");
	return (openCommands(user));
}

int	closeContainer(const char *user)
{
	char	*buffer;

	if ((buffer = malloc(sizeof(char) * (513))) == NULL)
		return (-1);
	if (sprintf(buffer, "umount -f %s/%s", getHome(user), CONTAINER) < 0)
		return (-1);
	if (system(buffer) < 0)
		return (-1);
	free(buffer);

	if ((buffer = malloc(sizeof(char) * (513))) == NULL)
		return (-1);
	if (sprintf(buffer, "cryptsetup luksClose %s_%s", user, CONTAINER) < 0)
		return (-1);
	if (system(buffer) < 0)
		return (-1);
	free(buffer);

	if ((buffer = malloc(sizeof(char) * (513))) == NULL)
		return (-1);
	if (sprintf(buffer, "rm -rf %s/%s", getHome(user), CONTAINER) < 0)
		return (-1);
	if (system(buffer) < 0)
		return (-1);
	free(buffer);

	return (0);
}
