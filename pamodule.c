#include "my.h"

void	cleanSetData(pam_handle_t *pamh, void *data, int error_status)
{
	(void)pamh;
	(void)error_status;
	if (data != NULL)
	{
		free(data);
	}
}

PAM_EXTERN int		pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
	const char		*user;
	const char   		*password;

	(void)flags;
	(void)argc;
	(void)argv;
	if (pam_get_item(pamh, PAM_AUTHTOK, (const void **)&password) != PAM_SUCCESS)
		return (PAM_AUTH_ERR);
	if (pam_get_user(pamh, &user, NULL) != PAM_SUCCESS)
		return (PAM_AUTH_ERR);
	if (pam_set_data(pamh, "pam_user_pass", strdup(password), &cleanSetData) != PAM_SUCCESS)
		return (PAM_AUTH_ERR);
	printf("Connected testest\n");
	return (PAM_SUCCESS);
}

PAM_EXTERN int		pam_sm_open_session(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
	const char		*user;
	const char		*password;

	(void)flags;
	(void)argc;
	(void)argv;
	printf("Openning your container...\n");
	if (pam_get_data(pamh, "pam_user_pass", (const void **)&password) != PAM_SUCCESS)
		return (PAM_SESSION_ERR);
	if (pam_get_user(pamh, &user, NULL) != PAM_SUCCESS)
		return (PAM_SESSION_ERR);
	if (openContainer(user, password) != 0)
		return (PAM_SESSION_ERR);
	printf("Container at %s\n", getHome(user));
	return (PAM_SUCCESS);
}

PAM_EXTERN int		pam_sm_close_session(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
	const char		*user;

	(void)flags;
	(void)argc;
	(void)argv;
	if (pam_get_user(pamh, &user, NULL) != PAM_SUCCESS)
		return (PAM_SESSION_ERR);
	if (closeContainer(user) != 0)
		return (PAM_SESSION_ERR);
	printf("Container closed.\n");
	return (PAM_SUCCESS);
}

PAM_EXTERN int		pam_sm_chauthtok(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
	const char		*password;
	const char		*newPassword;
	const char		*user;
	int			value;

	(void)flags;
	(void)argc;
	(void)argv;
	if ((value = pam_get_user(pamh, &user, NULL)) != PAM_SUCCESS)
		return (value);
	if ((value = pam_get_item(pamh, PAM_AUTHTOK, (const void **)&newPassword)) != PAM_SUCCESS)
		return (value);
	if (newPassword != NULL)
	{
		if ((value = pam_get_item(pamh, PAM_OLDAUTHTOK, (const void **)&password)) != PAM_SUCCESS)
			return (value);
		if (changePassword(user, password, newPassword) != 0)
			return (PAM_AUTHTOK_ERR);
		printf("Password changed.\n");
	}
	return (PAM_SUCCESS);
}

PAM_EXTERN int	pam_sm_acct_mgmt(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
	(void)pamh;
	(void)flags;
	(void)argc;
	(void)argv;
	return (PAM_SUCCESS);
}

PAM_EXTERN int	pam_sm_setcred(pam_handle_t* pamh, int flags, int argc, const char **argv)
{
	(void)pamh;
	(void)flags;
	(void)argc;
	(void)argv;
	return (PAM_SUCCESS);
}
