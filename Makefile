SRC =   pamodule.c \
	    container.c \
	    crypt.c \

NAME =  mypam.so

OBJS =	$(SRC:.c=.o)

CFLAGS += -fPIC -fno-stack-protector -W -Werror -Wall -Wextra

LDFLAGS =  -lcryptsetup

LIBCRYPTEXIST := $(shell command -v cryptsetup 2> /dev/null)

all: $(NAME)

$(NAME): $(OBJS)
	gcc $(OBJS) -shared -rdynamic $(LDFLAGS) -o $(NAME)

clean:
	rm -rf $(OBJS)

fclean: clean
	rm -rf $(NAME)
	rm -rf test

test:
	gcc -o test main.c -lpam -lpam_misc

install:
ifneq ("$(wildcard $(HOME)/.security)", "")
	@printf "Security folder already created.\n"
else
	mkdir $(HOME)/.security
	mkdir $(HOME)/.security/.oldauth
	@printf "Security folder created correctly.\n"
endif
ifneq ("$(wildcard /lib/security/)", "")
	@printf "Lib folder already created.\n"
else
	mkdir /lib/security/
	@printf "Lib folder created correctly.\n"
endif

ifneq ("$(wildcard /lib/security/mypam.so)", "")
	@printf "installed.\n"
else
	make
	cp $(NAME) /lib/security/$(NAME)
	cp /etc/pam.d/common-password $(HOME)/.security/.oldauth/
	cp /etc/pam.d/common-session $(HOME)/.security/.oldauth/
	cp /etc/pam.d/common-auth $(HOME)/.security/.oldauth/
	cp /etc/pam.d/common-account $(HOME)/.security/.oldauth/
	@echo "password optional mypam.so" >> /etc/pam.d/common-password
	@echo "session optional mypam.so" >> /etc/pam.d/common-session
	@echo "auth optional mypam.so" >> /etc/pam.d/common-auth
	@echo "account optional mypam.so" >> /etc/pam.d/common-account
	@printf "installed.\n"
endif

uninstall:
ifneq ("$(wildcard $(HOME)/.security)", "")
	cp $(HOME)/.security/.oldauth/common-password /etc/pam.d/
	cp $(HOME)/.security/.oldauth/common-session /etc/pam.d/
	cp $(HOME)/.security/.oldauth/common-auth /etc/pam.d/
	cp $(HOME)/.security/.oldauth/common-account /etc/pam.d/
	rm -rf $(HOME)/.security
	rm -rf /lib/security/
	make clean
	@printf "uninstalled.\n"
else
	@printf "not installed.\n"
endif

check:
ifeq ("$(wildcard $(HOME)/.security)", "")
	@printf "not installed.\n"
else
	@printf "installed.\n"
endif

re: clean all

