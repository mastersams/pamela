
#ifndef MY_H
#define MY_H

#include <security/pam_appl.h>
#include <security/pam_modules.h>
#include <security/pam_ext.h>
#include <libcryptsetup.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pwd.h>


#define CONTAINER "secure_data-rw"

char	*getHome(const char *user);
char	*getKey(const char *user);
int     openContainer(const char *user, const char *password);
int     closeContainer(const char *user);
int     createContainer(const char *user, const char *password);
int     changePassword(const char *user, const char *old, const char *new);
int     my_decrypt(const char *user, const char *password);
int     my_crypt(const char *user, const char *password);

#endif // _H_!MY_H_
