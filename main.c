#include <security/pam_appl.h>
#include <security/pam_modules.h>
#include <security/pam_misc.h>
#include <stdio.h>

const struct pam_conv conv =
{
  misc_conv,
  NULL
};

int	main(int argc, char **argv)
{
    pam_handle_t *pamh = NULL;
    int retval;
    const char *user;

    if (argc != 2)
    {
        printf("Usage: %s username\n", argv[0]);
        return (-1);
    }

    user = argv[1];

    retval = pam_start("check_user", user, &conv, &pamh);

    if (retval == PAM_SUCCESS)
    {
        printf("Credential OK\n");
        retval = pam_authenticate(pamh, 0);
    }

    if (retval == PAM_SUCCESS)
    {
        printf("Account OK\n");
        retval = pam_acct_mgmt(pamh, 0);
    }

    if (retval == PAM_SUCCESS)
        printf("Authentifie\n");
    else
        printf("User or password invalid\n");

    if (pam_end(pamh, retval) != PAM_SUCCESS)
    {
        pamh = NULL;
        printf("Failed to auhentify\n");
        return (0);
    }

    return (0);
}
