#include "my.h"

int			my_decrypt(const char *user, const char *password)
{
	struct crypt_device	*ctx;
	char			*userContainer;

	if ((userContainer = malloc(sizeof(char) * (513))) == NULL)
		return (-1);
	if (sprintf(userContainer, "%s_%s", user, CONTAINER) < 0)
		return (-1);
	if (crypt_init(&ctx, getKey(user)) < 0)
		return (-1);
	if (crypt_load(ctx, CRYPT_LUKS1, NULL) < 0)
		return (-1);
	if (crypt_activate_by_passphrase(ctx, userContainer, CRYPT_ANY_SLOT, password, strlen(password), 0) < 0)
		return (-1);
	crypt_free(ctx);
	return (0);
}

int	my_crypt(const char *user, const char *password)
{
	struct crypt_device           *ctx;
	struct crypt_params_luks1     params;

	if (crypt_init(&ctx, getKey(user)) < 0)
		return (-1);
	params.data_device = NULL;
	params.data_alignment = 0;
	params.hash = "sha1";
	if (crypt_format(ctx, CRYPT_LUKS1, "aes", "xts-plain64", NULL, NULL, 256 / 8, &params) < 0)
		return (-1);
	if (crypt_keyslot_add_by_volume_key(ctx, CRYPT_ANY_SLOT, NULL, 0, password, strlen(password)) < 0)
		return (-1);
	crypt_free(ctx);
	return (0);
}

int				changePassword(const char *user, const char *old, const char *new)
{
	uid_t				olduid;
	struct crypt_device		*device;

	olduid = getuid();
	if ((setuid(0) != 0))
		return (-1);
	if (crypt_init(&device, getKey(user)) < 0)
		return (-1);
	if (crypt_load(device, CRYPT_LUKS1, NULL) < 0)
		return (-1);
	if (crypt_keyslot_add_by_passphrase(device, CRYPT_ANY_SLOT, old, strlen(old), new, strlen(new)) < 0)
		return (-1);
	crypt_free(device);
	if (setuid(olduid) != 0)
		return (-1);
	return (0);
}
